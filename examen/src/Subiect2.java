import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class Subiect2 {

    public static void main(String args[]){
        JFrame frame = new JFrame();
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setSize(500,500);
        JPanel jp= new JPanel();
        JTextField field = new JTextField(20);
        JTextArea area = new JTextArea(5,20);
        JButton button = new JButton("OK");
        button.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                String string  = field.getText();
                area.setText(string);
            }
        });
        jp.add(field);
        jp.add(area);
        jp.add(button);
        frame.getContentPane().add(jp);

        frame.setVisible(true);
    }

}

